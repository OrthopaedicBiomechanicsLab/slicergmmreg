set(proj gmmreg)

# Set dependency list
set(${proj}_DEPENDS "vxl")



# Include dependent projects if any
ExternalProject_Include_Dependencies(${proj} PROJECT_VAR proj DEPENDS_VAR ${proj}_DEPENDS)

if(${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj})
  message(FATAL_ERROR "Enabling ${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj} is not supported !")
endif()

# Sanity checks
if(DEFINED gmmreg_DIR AND NOT EXISTS ${gmmreg_DIR})
  message(FATAL_ERROR "gmmreg_DIR variable is defined but corresponds to nonexistent directory")
endif()

if(NOT DEFINED ${proj}_DIR AND NOT ${CMAKE_PROJECT_NAME}_USE_SYSTEM_${proj})

  if(NOT DEFINED git_protocol)
    set(git_protocol "git")
  endif()

  set(${proj}_INSTALL_DIR ${CMAKE_BINARY_DIR}/${proj}-install)
  set(${proj}_DIR ${CMAKE_BINARY_DIR}/${proj}-build)

message(CMAKE_BINARY_DIR-Slicer_THIRDPARTY_LIB_DIR)
message(${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_LIB_DIR})
#message(${VXL_DIR}) - it does not exist
  
set(gmmreg_SRC_DIR ${CMAKE_BINARY_DIR}/${proj})
  
  ExternalProject_Add(${proj}
    # Slicer
    ${${proj}_EP_ARGS}
    SOURCE_DIR ${CMAKE_BINARY_DIR}/${proj}
    SOURCE_SUBDIR C++ # requires CMake 3.7 or later
    BINARY_DIR ${proj}-build
    INSTALL_DIR ${${proj}_INSTALL_DIR}
    GIT_REPOSITORY "https://bitbucket.org/OrthopaedicBiomechanicsLab/gmmreg.git"
    #GIT_TAG "a55d045cfc7f950a444b1a12469a8467fc0c4c23"
    #--Patch step-------------  
    PATCH_COMMAND ${CMAKE_COMMAND} -Delastix_SRC_DIR=${CMAKE_BINARY_DIR}/${proj}
      -P ${CMAKE_CURRENT_LIST_DIR}/${proj}_patch.cmake
    --Configure step-------------  
    CMAKE_CACHE_ARGS
      -DSubversion_SVN_EXECUTABLE:STRING=${Subversion_SVN_EXECUTABLE}
      -DGIT_EXECUTABLE:STRING=${GIT_EXECUTABLE}    
      -DVTK_DIR:PATH=${VTK_DIR}
	  -DVXL_DIR:PATH=${CMAKE_BINARY_DIR}/vxl-build
      -DCMAKE_CXX_COMPILER:FILEPATH=${CMAKE_CXX_COMPILER}
      -DCMAKE_CXX_FLAGS:STRING=${ep_common_cxx_flags}
      -DCMAKE_C_COMPILER:FILEPATH=${CMAKE_C_COMPILER}
      -DCMAKE_C_FLAGS:STRING=${ep_common_c_flags}
      -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
      -DBUILD_TESTING:BOOL=OFF
      -DCMAKE_MACOSX_RPATH:BOOL=0
      # location of elastix.exe and transformix.exe in the build tree:
	  #location of gmmreg_demo.exe and associated shared libs
      -DCMAKE_RUNTIME_OUTPUT_DIRECTORY:PATH=${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_BIN_DIR}
      -DCMAKE_LIBRARY_OUTPUT_DIRECTORY:PATH=${CMAKE_BINARY_DIR}/${Slicer_THIRDPARTY_LIB_DIR}
      -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY:PATH=${CMAKE_ARCHIVE_OUTPUT_DIRECTORY} 
      -DGMMREG_RUNTIME_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
      -DGMMREG_LIBRARY_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
	  -DGMMREG_ARCHIVE_DIR:STRING=${Slicer_INSTALL_THIRDPARTY_LIB_DIR}
    #--Build step-----------------
    #--Install step-----------------
    # Don't perform installation at the end of the build
    INSTALL_COMMAND ""
    DEPENDS
      ${${proj}_DEPENDS}
    )
  #set(${proj}_DIR ${${proj}_INSTALL_DIR})
  #if(UNIX)
  #  set(${proj}_DIR ${${proj}_INSTALL_DIR}/share/elastix)
  #endif()

else()
  ExternalProject_Add_Empty(${proj} DEPENDS ${${proj}_DEPENDS})
endif()

mark_as_superbuild(${proj}_DIR:PATH)
