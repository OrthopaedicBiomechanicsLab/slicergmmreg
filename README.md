# SlicerGmmReg
This extension is used to deformably register surface models by making available Robust Point Set Registration Using Gaussian Mixture Models (https://github.com/bing-jian/gmmreg) in Slicer.

Dependencies:

- [3D Slicer](https://github.com/Slicer/Slicer)
- [cuda 8](https://developer.nvidia.com/cuda-80-ga2-download-archive) - a direct link to cuda for windows (https://developer.nvidia.com/compute/cuda/8.0/Prod2/patches/2/cuda_8.0.61.2_windows-exe)
- [QT](https://www.qt.io/) - direct link to qt5 used in the compilation of slicer (https://download.qt.io/official_releases/qt/5.10/5.10.0/qt-opensource-windows-x86-5.10.0.exe)
- [Visual Studio 2015](https://my.visualstudio.com/Downloads?q=visual%20studio%202015&wt.mc_id=o~msft~vscom~older-downloads)


### How do I get set up? ###

- Install [git](https://github.com/git-for-windows/git/releases/download/v2.7.0.windows.1/Git-2.7.0-64-bit.exe)
- Install [cmake 3.7.2](https://cmake.org/files/dev/) or later. 
- Install [SlikSvn] (https://sliksvn.com/download/)

**Build Slicer**

General instructions for [building from source](https://www.slicer.org/slicerWiki/index.php/Documentation/Nightly/Developers/Build_Instructions#Windows) are available on the Slicer website. I would also have a look at the build scripts in [utility](https://bitbucket.org/OrthopaedicBiomechanicsLab/slicergmmreg/src/master/utility/) directory of this repo for scripts useful for building slicer and the extension.

There were two major build errors that we encountered when trying to build Slicer. Please update them if you find more on your machine.
1. Git is not able to fetch the submodule needed to build from RapidJSON. It seems like some versions of git may cause this error. This error was fixed by updating the git of the machine to the latest version. (2.17.0 at the time of documentation)
2. We found that if Anaconda is installed, it cannot properly find the library that it beeds (optimized.lib). Deleting Anaconda off of the computer fixed the error.

**Build SlicerGmmReg Modules**


1. Begin by cloning the repository.
```bash
git clone https://bitbucket.org/OrthopaedicBiomechanicsLab/slicergmmreg.git
```
2. Checkout the latest stable version of the repository.
```bash
git checkout master
```
3. Create a build directory (e.g., slicerGmmReg-r).
4. Open the CMake GUI. Set the source code (i.e., "Where is the source code") to the Calavera source directory (i.e., .../slicergmmreg). Set the build directory (i.e., "Where to build the binaries") to the empty build directory you previously created (e.g., .../slicergmmreg-r).
5. Press Configure. A popup window will appear asking you to "specify the generator for this project". Select "Visual Studio 14 2015 Win64", and ensure that the "Use default native compilers" radio button is selected".
7. Press Generate. 
8. Open the build directory (e.g., .../slicergmmreg-r). Open SlicerGmmReg.sln. This should open in Visual Studio 2015. 
9. The configuration value will likely read "Debug". Change it to "Release". This step is important, as the Configuration type must match that which was used to build Slicer. 
10. Click on the "Build" tab. Then click "Build solution". In my experience, the build takes approximately 10 minutes to complete.
 
**Link SlicerGmmReg Modules to Slicer**

Please note that a few of the modules will not work properly if the deblurring modules are not linked to Slicer as well (as their functionality is needed).

1. Open Slicer.exe from the super-build directory. 
2. Click on Edit --> Application Settings --> Modules. In the cli-modules file, drag and drop the release file into the "Additional modules paths" field. 
3. Restart Slicer. The GmmReg module should now be present in the Modules tab.

