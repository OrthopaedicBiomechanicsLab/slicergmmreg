#include <stdio.h>

class GmmRegistrationIniFileWriter
{
public:
  
  GmmRegistrationIniFileWriter() {
	modelFileLocation = "c:/temp/model.stl";
	sceneFileLocation = "c:/temp/scene.stl";
	controlPointsLocation; = "c:/temp/ctrl_pts.stl";
	finalRigidTransformLocation = "c:/temp/final_rigid.txt;
	finalAffineTransformLocation = "c:/temp/final_affine.txt;
	finalTpsTransformLocation = "c:/temp/final_tps.txt;
	finalParamsLocation = "c:/temp/final_params.txt";
	transformedModelLocation = "c:/temp/transformed_model.stl";
	

	level = 1;
	fixAffineFlag.assign(level,false);
	maxFunctionEvals.assign(level, 5);


	normalize = true;
	sigma.assign(level, 1.0);
	lambda.assign(level, 1.0);

	outliers = true;
	beta = 1;
	annealingRate = 0.97;
	tolerance = 1e-18;
	emTolerance = 1e-15;
	maxIterations = 5;
	maxEmIterations = 5;
  }
  ~GmmRegistrationIniFileWriter() {}
  int write();

  //Accessors for setting the coeffecients
  void SetCoeffecientRed(const OutputRealType & coeffecientRed)
  {
	  m_coeffecientRed = coeffecientRed;
  }
  void SetCoeffecientGreen(const OutputRealType & coeffecientGreen)
  {
	  m_coeffecientGreen = coeffecientGreen;
  }
  void SetCoeffecientBlue(const OutputRealType & coeffecientBlue)
  {
	  m_coeffecientBlue = coeffecientBlue;
  }
  void SetOffset(const OutputRealType & offset)
  {
	  m_offset = offset;
  }


  //Defines the pixelwise mapping
  inline TOutput operator()(const TInput & A) const
  {
	  return static_cast< TOutput >(m_coeffecientRed*A.GetRed() + m_coeffecientGreen*A.GetGreen() + m_coeffecientBlue*A.GetBlue() + m_offset);
  }

private:
	//[FILES]
	//# The alignment is done by moving the 'model' towards the fixed 'scene'
	//# Usually the point set with less points is chosen as the 'model'.
	std::string modelFileLocation;
	std::string sceneFileLocation;

	//# 'ctrl_pts' serve as the control points when thin plate splines (TPS) or
	//# Gaussian radial basis functions (GRBF) are used in the nonrigid registration
	//# if 'ctrl_pts' is not provided, model will be used as ctrl_pts
	//# the program 'gmmreg_aux' can be used to generate ctrl pts from regular grid pts
	std::string controlPointsLocation;

	//not implimented
	/*
	//# Initial transformation parameters. If not provided, default parameters
	//# corresponding to the identity transform will be used.

	init_affine =
	init_tps =
	init_params =
	*/

	//# Final output parameters

	//# rigid registration (2D: x,y,theta;  3D: quaternion + translation)
	std::string finalRigidTransformLocation;

	//# thin-plate splines which can be decomposed by affine and nonlinear part
	std::string finalAffineTransformLocation;
	std::string finalTpsTransformLocation;

	//# parameters corresponding to the weights of Gaussian radial basis functions
	std::string finalParamsLocation;

	//# The final transformed model is saved here
	std::string transformedModelLocation;


	/*[GMMREG_OPT]
	# This section configures parameters used in the point set registration methods
	# that minimize similarity measures between two Gaussian mixtures corresponding
	# to the two point sets by directly employing numerical optimization.
	#
	# The two representative references are:
	#   Yanghai Tsin and Takeo Kanade,
	#   A Correlation-Based Approach to Robust Point Set Registration,
	#   ECCV (3) 2004: 558-569.
	#
	#   Bing Jian and Baba C. Vemuri,
	#   A Robust Algorithm for Point Set Registration Using Mixture of Gaussians,
	#   ICCV 2005, pp. 1246-1251.
	#
	# Currently, 2D/3D rigid registration and nonrigid registration using thin plate
	# splines (TPS) and Gaussian radial basis functions (GRBF) are supported for above methods.
	*/

	//# if the 'normalize' flag is nonzero, normalization is done before the registration
	bool normalize;

	//# multiscale option, this number should be no more than the
	//# number of parameters given in options below
	int level;

	//# the scale parameters of Gaussian mixtures, from coarse to fine,
	std::vector<double> sigma;

	//# weights of the regularization term, e.g. the TPS bending energy
	std::vector<double> lambda;

	//# to fix the affine or not during optimization at each level
	std::vector<bool> fixAffineFlag;

	//# the max number of function evaluations at each level
	std::vector<int> maxFunctionEvals;


	/*[GMMREG_EM]
	# This section configures parameters used in the point set registration methods
	# based on the EM framework.
	#
	# The two representative references are:
	#  Haili Chui and Anand Rangarajan,
	#  A new point matching algorithm for non-rigid registration,
	#  Computer Vision and Image Understanding, 2003, 89(2-3), pp. 114-141.
	#
	#  Andriy Myronenko, Xubo B. Song, Miguel A. Carreira-Perpinan,
	#  Non-rigid Point Set Registration: Coherent Point Drift,
	#  NIPS 2006, pp. 1009-1016.
	#
	# Currently only 2D/3D nonrigid registation have been implemented based on above two methods.
	# Note that in Chui and Rangrajan (2000) the thin plate splines (TPS) model is used while the
	# Gaussian radial basis functions (GRBF) model is used in Myronenko et al. (2006).
	*/

	//# if the 'normalize' flag is nonzero, normalization is done before the registration
	//member already exists above normalize = 1

	//# account for outliers and missing points
	bool outliers;

	//# the initial scale for Gaussian radial basis functions
	//member already implimented above sigma = 1.0

	//# std of Gaussian kernel. Smaller value allows local deformations. Large - almost rigid
	double beta;

	//# weight of regularization
	//membor already implimented above lambda = 1

	//# annealing rate
	double annealingRate;

	//# for parameters below, pls. see http://www.csee.ogi.edu/~myron/matlab/cpd/
	double tolerance;
	double emTolerance;

	int maxIterations;
	int maxEmIterations;
};