<?xml version="1.0" encoding="utf-8"?>
<executable>
  <category>Examples</category>
  <title>GmmRegistration</title>
  <description>
    <![CDATA[<u>Source</u> <br /> This module is a modified version of the wrapping of Gaussian Mixture based Registration implimented at https://github.com/bing-jian/gmmreg. <br /> <br /> <u>Overview</u> <br /> This module has the ability to: <br />  1) Deformably register a model to a scene, output the transformed model and save the corresponding transform. <br />  2) Apply this corresponding transform or a prior transform derived from deformable registration to a model (e.g. higher res model). <br /> <br />  <u>Deformable Registration Notes</u> <br /> If the deformable registration type is EM_GRBF, only the following parameter values have an effect on the outcome: normalize, outliers, sigma, lambda, beta, anneal, tol, emtol, max_iterations, max_em_iterations. <br />  Please refer to the following parameter study on confluence to better understand how these parameters impact the registration: <br /> Deformable registration does not take into account an input transform, only a model, scene, control points (default = model), and outputs a transformed model and an outputted transform. <br /> <br /> <u>Tranform Application Notes</u> <br /> If one is applying a transform to a model (2nd functionality listed above), Init Params must be set to the .txt file that contains the desired transform. Control points should always be set, or as a default they will be set as the model itself. The scene does not matter in applying the transform, only model, control points, init params and transformed model matter. <br /> <br /> <u>Outline of Input Parameters</u> <br />  <b>No Registration:</b> Flag not thrown = GMMREG, Flag thrown = Transform Application. <br /> <b>Model:</b> Object which will undergo GMMREG or to which transform will be applied. <br /> <b>Scene:</b> Object that the model will aim to deform to for GMMREG. <br /> <b>Control Points:</b> A list of fiducial points or .stl file that determine the control points within the GMM based deformable of the surfaces. Defining control points will affect the outcome of registration. If Control Points aren't set then model is used as the control points. <br /> <b>Input Transform:</b> A .txt file used only in Transform Application - contains a previously outputted transform from GMMREG to apply to a new model. <br /> <b>Outputted Transform:</b> The name of the .txt file (.txt extension will be automatically added, only write the name of file) in which the outputted transform of GMMREG will be saved. <br /> <br /> <u>Transform Application to Higher Res Models</u> <br /> After obtaining a transform from a lower res GMMREG, set the low res model (untransformed) as the control points, the high res model as model, scene does not matter, and choose the InputTransform .txt file that contains the transform derived from lowres GMMREG. <br />]]>
</description>
  <version>0.0.1</version>
  <documentation-url>http://www.example.com/Slicer/Modules/GmmRegistration</documentation-url>
  <license>Slicer</license>
  <contributor>FirstName LastName (Institution), FirstName LastName (Institution)</contributor>
  <acknowledgements>This work was partially funded by NIH grant NXNNXXNNNNNN-NNXN</acknowledgements>
  <parameters>
    <label>IO</label>
    <description><![CDATA[Input/output parameters]]></description>
	  <boolean>
	      <name>noRegistration</name>
        <longflag>noRegistration</longflag>
        <label>No Registration</label>
        <description><![CDATA[If flag is not thrown (default) GMMREG is performed. If flag is thrown, GMMReg is not performed and instead initparams file are used to apply transform from previous registration to different model (ex. higher res)]]></description>
        <default>false</default>
    </boolean>
	<geometry>
      <name>controlPoints</name>
      <label>Control Points</label>
      <channel>input</channel>
      <index>0</index>
      <description><![CDATA[Control Points, a list of fiducial pointsthat determine the control points within the GMM based deformable or affine registration of the surfaces.  Defining control points will affect the outcome of registration and will greatly reduce the time for registration. If Control Points aren't set then model is used as the control points]]></description>
      <default>None</default>
    </geometry>
    <geometry>
      <name>model</name>
      <label>Model</label>
      <channel>input</channel>
      <index>1</index>
      <default>None</default>
      <description><![CDATA[]]></description>
    </geometry>
		<geometry>
      <name>sceneGmm</name>
      <label>SceneGmm</label>
      <channel>input</channel>
      <index>2</index>
      <default>None</default>
      <description><![CDATA[]]></description>
    </geometry>
	  
  <file fileExtensions =".txt">
      <name>initParams</name>
      <label>Input Transform</label>
      <channel>input</channel>
      <index>3</index>
      <default>c:\\temp\\outputted_transform.txt</default>
      <description><![CDATA[.txt file containing initial parameters used in applying transform derived from prior GMMREG to other models]]></description>
  </file>
    <string>
      <name>finalParams</name>
      <label>Output Transform</label>
      <channel>input</channel>
      <index>4</index>
      <default>outputted_transform</default>
      <description><![CDATA[Name of .txt file where final parameters where transformed derived from GMMREG will be saved]]></description>
    </string>
    <geometry>
      <name>transformedModel</name>
      <label>Transformed Model</label>
      <channel>output</channel>
      <index>5</index>
      <default>None</default>
      <description><![CDATA[Transformed version of 'model' geometry to the scene geometry]]></description>
    </geometry>
	<string-enumeration>
      <name>registrationMethod</name>
      <label>Registration Method</label>
      <description><![CDATA[Method of deformable registration.]]></description>
      <longflag>--RegMethod</longflag>
      <flag>--r</flag>
	  <!--'EM_TPS': Haili Chui and Anand Rangarajan, A new point matching algorithm for n
      n-rigid registration, Computer Vision and Image Understanding, 2003, 89(2-3), p. 114-141.
      'EM_GRBF': Andriy Myronenko, Xubo B. Song, Miguel A. Carreira-Perpinan, Non-rigid Point Set Registration: Coherent Point Drift,NIPS 2006, pp. 1009-1016.
      'TPS_L2, GRBF_L2': Bing Jian and Baba C. Vemuri, A Robust Algorithm for Point Set Registration Using Mixture of Gaussians, ICCV 2005, pp. 1246-1251.
      'TPS_KC, GRBF_KC': Yanghai Tsin and Takeo Kanade, A Correlation-Based Approach to Robust Point Set Registration, ECCV (3) 2004: 558-569.
      'rigid':  rigid registration using Jian and Vemuri's algorithm.
	  -->
      <default>EM_GRBF</default>
      <element>EM_GRBF</element>
      <element>TPS_L2</element>
      <element>GRBF_L2</element>
      <element>TPS_KC</element>
      <element>GRBF_KC</element>
      <element>rigid</element>
    </string-enumeration>
	<boolean>
	  <name>normalize</name>
      <longflag>normalize</longflag>
      <flag>n</flag>
      <label>Normalize</label>
      <description><![CDATA[]]></description>
      <default>true</default>
	</boolean>
	<boolean>
	  <name>outliers</name>
      <longflag>outliers</longflag>
      <flag>o</flag>
      <label>Outliers</label>
      <description><![CDATA[Acount for outliers flag]]></description>
      <default>true</default>
	</boolean>
	<integer>
      <name>levels</name>
      <longflag>levels</longflag>
      <flag>l</flag>
      <label>levels</label>
      <description><![CDATA[]]></description>
      <default>4</default>
  </integer>
	<double>
      <name>sigmaLevelReduction</name>
      <longflag>sigmaLR</longflag>
      <label>Sigma Level Reduction Factor</label>
      <description><![CDATA[]]></description>
      <default>2.0</default>
    </double>
	<double>
      <name>sigmaStart</name>
      <longflag>sigmaStart</longflag>
      <flag>s</flag>
      <label>Sigma Start</label>
      <description><![CDATA[]]></description>
      <default>2.0</default>
    </double>
	<double>
      <name>lambdaLevelReduction</name>
      <longflag>lambdaLR</longflag>
      <label>Lambda Level Reduction Factor</label>
      <description><![CDATA[]]></description>
      <default>2.0</default>
    </double>
	<double>
      <name>lambdaStart</name>
      <longflag>lambdaStart</longflag>
      <label>Lambda Start</label>
      <description><![CDATA[]]></description>
      <default>2.0</default>
    </double>
	<double>
      <name>beta</name>
      <longflag>beta</longflag>
      <flag>b</flag>
      <label>beta</label>
      <description><![CDATA[std of Gaussian kernel. Smaller value allows local deformations. Large - almost rigid]]></description>
      <default>1.0</default>
    </double>
	<double>
      <name>anneal</name>
      <longflag>anneal</longflag>
      <flag>a</flag>
      <label>anneal</label>
      <description><![CDATA[GMMREG_EM: annealing rate]]></description>
      <default>0.97</default>
    </double>
	<double>
      <name>tol</name>
      <longflag>tol</longflag>
      <flag>t</flag>
      <label>Tolerance</label>
      <description><![CDATA[]]></description>
      <default>1e-18</default>
    </double>
	<double>
      <name>emtol</name>
      <longflag>emtol</longflag>
      <flag>e</flag>
      <label>em Tolerance</label>
      <description><![CDATA[]]></description>
      <default>1e-15</default>
    </double>
    <integer>
      <name>iterationsLevelReduction</name>
      <longflag>iterationsLevelReduction</longflag>
      <label>Iterations Level Reduction</label>
      <description><![CDATA[]]></description>
      <default>1</default>
    </integer>
	<integer>
      <name>iterationsStart</name>
      <longflag>iterationsStart</longflag>
      <flag>i</flag>
      <label>Iterations Start</label>
      <description><![CDATA[]]></description>
      <default>3</default>
    </integer>
	<integer>
      <name>maxEmIterations</name>
      <longflag>maxEmIterations</longflag>
      <label>Maximum em Iterations</label>
      <description><![CDATA[GMMREG_EM: annealing rate]]></description>
      <default>3</default>
    </integer>
  </parameters>
</executable>
