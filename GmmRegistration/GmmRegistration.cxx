// VTK includes
#include <vtkAlgorithmOutput.h>
#include <vtkAppendPolyData.h>
#include <vtkDebugLeaks.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkSTLReader.h>
#include <vtkSTLWriter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkVersion.h>
#include <vtksys/SystemTools.hxx>
#include <vtkSmartPointer.h>

#include "GmmRegistrationCLP.h"
#include "whereami.h"


#include <iostream>
#include <fstream>

#include <array>

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//

class myClassWithInit {
	int myMember = 0;
};

class IniFileWriter {



public:

	IniFileWriter();
	IniFileWriter(bool noRegistration, const std::string& control_points_file_location, const std::string& model_file_location, const std::string& scene_file_location, const std::string& init_params_file_location, const std::string& final_params_file_location, bool normalize, bool outliers, int levels, double sigma_level_reduction, double sigma_start, double lambda_level_reduction, double lambda_start, int iterations_level_reduction, double beta, double anneal, double tol, double emtol, int max_em_iterations, const std::string& transformed_model_file_location);

	void writeFile();

	bool getNoRegistration() const
	{
		return noRegistration;
	}

	void setNoRegistration(bool noRegistration)
	{
		this->noRegistration = noRegistration;
	}
	std::string getControlPointsFileLocation() const
	{
		return controlPointsFileLocation;
	}

	void setControlPointsFileLocation(const std::string& control_points_file_location)
	{
		controlPointsFileLocation = control_points_file_location;
	}

	std::string getModelFileLocation() const
	{
		return modelFileLocation;
	}

	void setModelFileLocation(const std::string& model_file_location)
	{
		modelFileLocation = model_file_location;
	}

	std::string getSceneFileLocation() const
	{
		return sceneFileLocation;
	}

	void setSceneFileLocation(const std::string& scene_file_location)
	{
		sceneFileLocation = scene_file_location;
	}
	std::string getInitParamsFileLocation() const
	{
		return initParamsFileLocation;
	}

	void setInitParamsFileLocation(const std::string& init_params_file_location)
	{
		initParamsFileLocation = init_params_file_location;
	}

	std::string getFinalParamsFileLocation() const
	{
		return finalParamsFileLocation;
	}

	void setFinalParamsFileLocation(const std::string& final_params_file_location)
	{
		finalParamsFileLocation = final_params_file_location;
	}

	bool getNormalize() const
	{
		return normalize;
	}

	void setNormalize(bool normalize)
	{
		this->normalize = normalize;
	}

	bool getOutliers() const
	{
		return outliers;
	}

	void setOutliers(bool outliers)
	{
		this->outliers = outliers;
	}

	int getLevels() const
	{
		return levels;
	}

	void setLevels(int levels)
	{
		this->levels = levels;
	}

	double getSigmaLevelReduction() const
	{
		return sigmaLevelReduction;
	}

	void setSigmaLevelReduction(double sigma_level_reduction)
	{
		sigmaLevelReduction = sigma_level_reduction;
	}

	double getSigmaStart() const
	{
		return sigmaStart;
	}

	void setSigmaStart(double sigma_start)
	{
		sigmaStart = sigma_start;
	}

	double getLambdaLevelReduction() const
	{
		return lambdaLevelReduction;
	}

	void setLambdaLevelReduction(double lambda_level_reduction)
	{
		lambdaLevelReduction = lambda_level_reduction;
	}

	double getLambdaStart() const
	{
		return lambdaStart;
	}

	void setLambdaStart(double lambda_start)
	{
		lambdaStart = lambda_start;
	}

	int getIterationsLevelReduction() const
	{
		return iterationsLevelReduction;
	}

	void setIterationsLevelReduction(int iterations_level_reduction)
	{
		iterationsLevelReduction = iterations_level_reduction;
	}

	double getBeta() const
	{
		return beta;
	}

	void setBeta(double beta)
	{
		this->beta = beta;
	}

	double getAnneal() const
	{
		return anneal;
	}

	void setAnneal(double anneal)
	{
		this->anneal = anneal;
	}

	double getTol() const
	{
		return tol;
	}

	void setTol(double tol)
	{
		this->tol = tol;
	}

	double getEmtol() const
	{
		return emtol;
	}

	void setEmtol(double emtol)
	{
		this->emtol = emtol;
	}

	int getMaxEmIterations() const
	{
		return maxEmIterations;
	}

	void setMaxEmIterations(int max_em_iterations)
	{
		maxEmIterations = max_em_iterations;
	}

	std::string getTransformedModelFileLocation() const
	{
		return transformedModelFileLocation;
	}

	void setTransformedModelFileLocation(const std::string& transformed_model_file_location)
	{
		transformedModelFileLocation = transformed_model_file_location;
	}


	int getMaxIterations() const
	{
		return maxIterations;
	}

	void setMaxIterations(int max_iterations)
	{
		maxIterations = max_iterations;
	}
	std::string getIniFileLocation() const
	{
		return iniFileLocation;
	}

	void setIniFileLocation(const std::string& ini_file_location)
	{
		iniFileLocation = ini_file_location;
	}

private:
	std::string iniFileLocation;
	std::string controlPointsFileLocation;
	std::string modelFileLocation;
	std::string	sceneFileLocation;
	std::string initParamsFileLocation;
	std::string finalParamsFileLocation;

	//RegistrationMethodType registrationMethod;
	bool noRegistration;
	bool normalize;
	bool outliers;
	int levels;
	double sigmaLevelReduction;
	double sigmaStart;
	double lambdaLevelReduction;
	double lambdaStart;
	int	iterationsLevelReduction;
	double beta;
	double anneal;
	double tol;
	double emtol;
	int maxIterations;
	int maxEmIterations;
	std::string transformedModelFileLocation;
};

IniFileWriter::IniFileWriter()
{
	controlPointsFileLocation = "";
	modelFileLocation = "";
	sceneFileLocation = "";
	initParamsFileLocation = "";
	finalParamsFileLocation = "";

	normalize = true;
	outliers = true;
	levels = 1;
	sigmaLevelReduction = 10;
	sigmaStart = 2;
	lambdaLevelReduction = 10;
	lambdaStart = 1;
	iterationsLevelReduction = 10;
	beta = 1;
	anneal = 0.97;
	tol = 1e-18;
	emtol = 1e-15;
	maxIterations = 5;
	maxEmIterations = 5;
	transformedModelFileLocation = "";
}

IniFileWriter::IniFileWriter(bool noRegistration, const std::string& control_points_file_location, const std::string& model_file_location, const std::string& scene_file_location, const std::string& init_params_file_location, const std::string& final_params_file_location, bool normalize, bool outliers, int levels, double sigma_level_reduction, double sigma_start, double lambda_level_reduction, double lambda_start, int iterations_level_reduction, double beta, double anneal, double tol, double emtol, int max_em_iterations, const std::string& transformed_model_file_location) : noRegistration(noRegistration), controlPointsFileLocation(control_points_file_location),
modelFileLocation(model_file_location),
sceneFileLocation(scene_file_location),
initParamsFileLocation(init_params_file_location),
finalParamsFileLocation(final_params_file_location),
normalize(normalize),
outliers(outliers),
levels(levels),
sigmaLevelReduction(sigma_level_reduction),
sigmaStart(sigma_start),
lambdaLevelReduction(lambda_level_reduction),
lambdaStart(lambda_start),
iterationsLevelReduction(iterations_level_reduction),
beta(beta),
anneal(anneal),
tol(tol),
emtol(emtol),
maxEmIterations(max_em_iterations),
transformedModelFileLocation(transformed_model_file_location)
{
}

void IniFileWriter::writeFile() {

	ofstream inifile;


	inifile.open(iniFileLocation);
	inifile << "#=========================================================================\n";
	inifile << "# Configuration INI File, automatically generated by from Slicer by GmmRegistration module\n";
	inifile << "#=========================================================================\n\n";
	inifile << "# Notes:\n";
	inifile << "# (1) Section names should be upper-case, e.g. use 'FILES' not 'Files'\n";
	inifile << "# (2) All keys should be lower-case, e.g. use 'model' but not 'Model'\n\n";
	inifile << "[FILES]\n\n";
	inifile << "noRegistration = " << noRegistration << "\n";
	inifile << "model = " << modelFileLocation << "\n";
	inifile << "scene = " << sceneFileLocation << "\n";
	inifile << "ctrl_pts = " << controlPointsFileLocation << "\n";
	inifile << "init_params = " << initParamsFileLocation << "\n";
	inifile << "final_params = " << finalParamsFileLocation << "\n";
	inifile << "transformed_model = " << transformedModelFileLocation << "\n";


	inifile << "[GMMREG_OPT]\n";
	inifile << "# if the 'normalize' flag is nonzero, normalization is done before the registration\n";
	inifile << "normalize = " << int(normalize) << std::endl;
	inifile << "# multiscale option, this number should be no more than the\n";
	inifile << "# number of parameters given in options below\n";
	inifile << "level = " << levels << "\n";


	inifile << "# the scale parameters of Gaussian mixtures, from coarse to fine,\n";
	//inifile << "sigma = 2.0 1.0 0.5 0.1" << sigmaLevelValues << "\n";
	inifile << "sigma = ";


	double sigmaValue;
	for (int sigmaIndex = 0; sigmaIndex < levels; sigmaIndex++)
	{
		sigmaValue = sigmaStart / pow(sigmaLevelReduction, sigmaIndex);
		inifile << sigmaValue << " ";
	}
	inifile << "\n";


	inifile << "# weights of the regularization term, e.g.the TPS bending energy\n";
	//inifile << "lambda = 1 1 0 0
	inifile << "lambda = ";


	double lambdaValue;
	for (int lambdaIndex = 0; lambdaIndex < levels; lambdaIndex++)
	{
		lambdaValue = lambdaStart / pow(lambdaLevelReduction, lambdaIndex);
		inifile << lambdaValue << " ";
	}
	inifile << "\n";


	inifile << "# to fix the affine or not during optimization at each level\n";
	//inifile << "fix_affine = 0 0 0 0
	inifile << "fix_affine = ";

	for (int affineIndex = 0; affineIndex < levels; affineIndex++)
	{
		inifile << 0 << " ";
	}
	inifile << "\n";

	inifile << "# the max number of function evaluations at each level\n";

	//inifile << "max_function_evals = 5 5 5 5";
	inifile << "max_function_evals = ";

	for (int maxFunctionIndex = 0; maxFunctionIndex < levels; maxFunctionIndex++)
	{
		inifile << maxIterations << " ";
	}
	inifile << "\n";

	inifile << "[GMMREG_EM]\n";
	inifile << "# This section configures parameters used in the point set registration methods\n";
	inifile << "# based on the EM framework.\n";
	inifile << "#\n";
	inifile << "# The two representative references are :\n";
	inifile << "#  Haili Chui and Anand Rangarajan,\n";
	inifile << "#  A new point matching algorithm for non - rigid registration,\n";
	inifile << "#  Computer Vision and Image Understanding, 2003, 89(2 - 3), pp. 114 - 141.\n";
	inifile << "#";
	inifile << "#  Andriy Myronenko, Xubo B.Song, Miguel A.Carreira - Perpinan,\n";
	inifile << "#  Non - rigid Point Set Registration : Coherent Point Drift,\n";
	inifile << "#  NIPS 2006, pp. 1009 - 1016.\n";
	inifile << "#";
	inifile << "# Currently only 2D / 3D nonrigid registation have been implemented based on above two methods.\n";
	inifile << "# Note that in Chui and Rangrajan(2000) the thin plate splines(TPS) model is used while the\n";
	inifile << "# Gaussian radial basis functions(GRBF) model is used in Myronenko et al. (2006).\n";

	inifile << "# if the 'normalize' flag is nonzero, normalization is done before the registration\n";
	inifile << "normalize = " << normalize << "\n";

	inifile << "# account for outliers and missing points\n";
	inifile << "outliers = " << outliers << "\n";

	inifile << "# the initial scale for Gaussian radial basis functions\n";
	inifile << "sigma = " << sigmaStart << "\n";

	inifile << "# std of Gaussian kernel.Smaller value allows local deformations.Large - almost rigid\n";
	inifile << "beta = " << beta << "\n";

	inifile << "# weight of regularization\n";
	inifile << "lambda = " << lambdaStart << "\n";

	inifile << "# annealing rate\n";
	inifile << "anneal = " << anneal << "\n";

	inifile << "# for parameters below, pls.see http ://www.csee.ogi.edu/~myron/matlab/cpd/\n";
	inifile << "tol = " << tol << "\n";
	inifile << "emtol = " << emtol << "\n";

	inifile << "max_iter = " << maxIterations << "\n";
	inifile << "max_em_iter = " << maxEmIterations << "\n";

	inifile << "\n";
	inifile << "\n";

	inifile.close();
}


template<typename T>
int vectorToVtkPolyData(const std::vector<T>& xyzPoints, vtkSmartPointer<vtkPolyData>& polyData) {
	//put xyzPoints defined in vector into polyData

	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

	for (int pointIndex = 0; pointIndex < xyzPoints.size(); pointIndex++)
	{
		points->InsertNextPoint(xyzPoints[pointIndex][0], xyzPoints[pointIndex][1], xyzPoints[pointIndex][2]);
	}

	polyData->SetPoints(points);

	return 1;

}




int main(int argc, char * argv[])
{
	PARSE_ARGS;

	std::string tempDirectoryLocation = "c:\\temp\\";
	std::string configureScriptLocation = tempDirectoryLocation + "autoGen.ini";
	std::string GmmRegistrationExecutableLocation;

	char* path = NULL;
	int length, dirname_length;
	int i;

	length = wai_getModulePath(NULL, 0, &dirname_length);
	if (length > 0)
	{
		path = (char*)malloc(length + 1);
		if (!path)
			abort();
		wai_getModulePath(path, length, &dirname_length);
		//path[length] = '\0';



		path[dirname_length] = '\0';
		GmmRegistrationExecutableLocation = path;
		GmmRegistrationExecutableLocation = GmmRegistrationExecutableLocation + "\\..\\gmmreg_demo.exe";
		free(path);
	}
	else {
		throw "Could not detect module path";
	}

	//std::string GmmRegistrationExecutableLocation = "C:\\Mike\\Git\\gmmreg-build-Release\\Release\\gmmreg_demo.exe";



	//std::string registrationMethodString = "TPS_L2";
	std::string registrationMethodString = registrationMethod;

	//No longer needed as control points can be .stl files directly now
	/*
	vtkSmartPointer<vtkPolyData> polyControlPoints = vtkSmartPointer<vtkPolyData>::New();
	if (controlPoints.size() > 0)
	{
	vectorToVtkPolyData(controlPoints, polyControlPoints);
	}
	*/


	vtkDebugLeaks::SetExitError(true);

	vtkSmartPointer<vtkPolyData> polyDataModel = NULL;
	vtkSmartPointer<vtkPolyData> polyDataScene = NULL;
	vtkSmartPointer<vtkPolyData> polyDataControlPoints = NULL;


	// do we have vtk or vtp models?
	std::string extension = vtksys::SystemTools::LowerCase(
		vtksys::SystemTools::GetFilenameLastExtension(model));
	if (extension.empty())
	{
		std::cerr << "Failed to find an extension for " << model << std::endl;
		return EXIT_FAILURE;
	}

	// read the first poly data
	if (extension == std::string(".vtk"))
	{
		vtkPolyDataReader* pdReaderModel = vtkPolyDataReader::New();
		pdReaderModel->SetFileName(model.c_str());
		pdReaderModel->Update();
		polyDataModel = pdReaderModel->GetOutput();
	}
	else if (extension == std::string(".vtp"))
	{
		vtkXMLPolyDataReader* pdxReaderModel = vtkXMLPolyDataReader::New();
		pdxReaderModel->SetFileName(model.c_str());
		pdxReaderModel->Update();
		polyDataModel = pdxReaderModel->GetOutput();
	}

	//this check should be figured out and added back
	/*if( readerModel->GetProducer()->GetErrorCode() != 0 )
	{
	std::cerr << "Failed to read model " << model << std::endl;
	return EXIT_FAILURE;
	}
	// read the second poly data
	*/

	extension = vtksys::SystemTools::LowerCase(vtksys::SystemTools::GetFilenameLastExtension(sceneGmm));
	if (extension.empty())
	{
		std::cerr << "Failed to find an extension for " << sceneGmm << std::endl;
		return EXIT_FAILURE;
	}
	if (extension == std::string(".vtk"))
	{
		vtkPolyDataReader* pdReaderScene = vtkPolyDataReader::New();
		pdReaderScene->SetFileName(sceneGmm.c_str());
		pdReaderScene->Update();
		polyDataScene = pdReaderScene->GetOutput();
	}
	else if (extension == std::string(".vtp"))
	{
		vtkXMLPolyDataReader* pdxReaderScene = vtkXMLPolyDataReader::New();
		pdxReaderScene->SetFileName(sceneGmm.c_str());
		pdxReaderScene->Update();
		polyDataScene = pdxReaderScene->GetOutput();
	}
	//this check should be figured out and put back
	/*
	if( readerScene->GetProducer()->GetErrorCode() != 0 )
	{
	std::cerr << "Failed to read scene " << scene << std::endl;
	return EXIT_FAILURE;
	}
	*/

	extension = vtksys::SystemTools::LowerCase(vtksys::SystemTools::GetFilenameLastExtension(controlPoints));
	if (extension.empty())
	{
		std::cerr << "Failed to find an extension for " << controlPoints << std::endl;
		return EXIT_FAILURE;
	}
	if (extension == std::string(".vtk"))
	{
		vtkPolyDataReader* pdReaderControlPoints = vtkPolyDataReader::New();
		pdReaderControlPoints->SetFileName(controlPoints.c_str());
		pdReaderControlPoints->Update();
		polyDataControlPoints = pdReaderControlPoints->GetOutput();
	}
	else if (extension == std::string(".vtp"))
	{
		vtkXMLPolyDataReader* pdxReaderControlPoints = vtkXMLPolyDataReader::New();
		pdxReaderControlPoints->SetFileName(controlPoints.c_str());
		pdxReaderControlPoints->Update();
		polyDataControlPoints = pdxReaderControlPoints->GetOutput();
	}


	// write models to disk

	vtkSmartPointer<vtkSTLWriter> writer = vtkSmartPointer<vtkSTLWriter>::New();
	std::string controlPointFileLocation = "";
	if (controlPoints.size() > 0)
	{
		writer->SetInputData(polyDataControlPoints);
		controlPointFileLocation = tempDirectoryLocation + "controlPoints.stl";
		writer->SetFileName(controlPointFileLocation.c_str());
		writer->Write();
	}


	writer->SetInputData(polyDataModel);
	std::string modelFileLocation = tempDirectoryLocation + "model.stl";
	writer->SetFileName(modelFileLocation.c_str());
	writer->Write();

	writer->SetInputData(polyDataScene);
	std::string sceneFileLocation = tempDirectoryLocation + "scene.stl";
	writer->SetFileName(sceneFileLocation.c_str());
	writer->Write();

	std::string transformedModelLocation = tempDirectoryLocation + "transformed_model.stl";
	std::string finalParamsFileLocation = tempDirectoryLocation + finalParams + ".txt";
	std::string initParamsFileLocation = initParams;

	// deformably register using GMM code via system call
	IniFileWriter iniGmmFileWriter;

	iniGmmFileWriter.setIniFileLocation(configureScriptLocation);
	iniGmmFileWriter.setNoRegistration(noRegistration);
	iniGmmFileWriter.setControlPointsFileLocation(controlPointFileLocation);
	iniGmmFileWriter.setModelFileLocation(modelFileLocation);
	iniGmmFileWriter.setSceneFileLocation(sceneFileLocation);
	iniGmmFileWriter.setInitParamsFileLocation(initParamsFileLocation);
	iniGmmFileWriter.setFinalParamsFileLocation(finalParamsFileLocation);

	iniGmmFileWriter.setNormalize(normalize);
	iniGmmFileWriter.setOutliers(outliers);
	iniGmmFileWriter.setLevels(levels);
	iniGmmFileWriter.setSigmaLevelReduction(sigmaLevelReduction);
	iniGmmFileWriter.setSigmaStart(sigmaStart);
	iniGmmFileWriter.setLambdaLevelReduction(lambdaLevelReduction);
	iniGmmFileWriter.setLambdaStart(lambdaStart);
	iniGmmFileWriter.setIterationsLevelReduction(iterationsLevelReduction);
	iniGmmFileWriter.setBeta(beta);
	iniGmmFileWriter.setAnneal(anneal);
	iniGmmFileWriter.setTol(tol);
	iniGmmFileWriter.setEmtol(emtol);
	iniGmmFileWriter.setMaxIterations(maxEmIterations);
	iniGmmFileWriter.setMaxEmIterations(maxEmIterations);
	iniGmmFileWriter.setTransformedModelFileLocation(transformedModelLocation);

	iniGmmFileWriter.writeFile();

	//delete transformed model
	remove(iniGmmFileWriter.getTransformedModelFileLocation().c_str());


	std::string systemCall = GmmRegistrationExecutableLocation + " " + configureScriptLocation + " " + registrationMethodString;
	cout << systemCall.c_str() << std::endl;
	cout << "system" << system(systemCall.c_str()) << std::endl;





	vtkSmartPointer<vtkSTLReader> readerTransformedModel = vtkSmartPointer<vtkSTLReader>::New();
	readerTransformedModel->SetFileName(transformedModelLocation.c_str());
	readerTransformedModel->Update();

	vtkSmartPointer<vtkPolyData> transformedModelPolyData;
	transformedModelPolyData = readerTransformedModel->GetOutput();

	// write the output
	extension = vtksys::SystemTools::LowerCase(vtksys::SystemTools::GetFilenameLastExtension(transformedModel));
	if (extension.empty())
	{
		std::cerr << "Failed to find an extension for " << transformedModel << std::endl;
		return EXIT_FAILURE;
	}
	if (extension == std::string(".vtk"))
	{
		vtkPolyDataWriter *pdWriter = vtkPolyDataWriter::New();
		pdWriter->SetFileName(transformedModel.c_str());
		pdWriter->SetInputConnection(readerTransformedModel->GetOutputPort());
		pdWriter->Write();
		pdWriter->Delete();
	}
	else if (extension == std::string(".vtp"))
	{
		vtkXMLPolyDataWriter *pdWriter = vtkXMLPolyDataWriter::New();
		pdWriter->SetIdTypeToInt32();
		pdWriter->SetFileName(transformedModel.c_str());
		pdWriter->SetInputConnection(readerTransformedModel->GetOutputPort());
		pdWriter->Write();
		pdWriter->Delete();
	}

	// clean up
	//add->Delete();
	//readerModel->GetProducer()->Delete();
	//readerScene->GetProducer()->Delete();
	return EXIT_SUCCESS;
}
