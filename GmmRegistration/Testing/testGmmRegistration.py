#The idea is that this test is run from the command line with something like
#  cd /c/Mike/Git/calavera-3d-slicer/GmmRegistration/Testing/
# /c/S4Dvs2013/Slicer-build/Slicer.exe --python-script testGmmRegistration.py
import time

slicer.util.loadModel("C:\Mike\Git\calavera-3d-slicer\GmmRegistration\Data\Input\smallPiecesRegistration\FillerModel#1.vtk")
fillerModelNode = getNode('FillerModel#1')
slicer.util.loadModel("C:\Mike\Git\calavera-3d-slicer\GmmRegistration\Data\Input\smallPiecesRegistration\BA_Small_viaMeshLab.vtk")
skullNode = getNode('BA_Small_viaMeshLab')

#parameters = {}
#parameters["model"] = fillerModelNode.GetID()
#parameters["sceneGmm"] = skullNode.GetID()

parameters = {}
parameters["model"] = fillerModelNode.GetID()
parameters["sceneGmm"] = skullNode.GetID()
#outModel = slicer.vtkMRMLModelNode()
#slicer.mrmlScene.AddNode( outModel )

#parameters["transformedModel"] = outModel.GetID()
parameters["normalize"] = 0
parameters["outliers"] = 0
parameters["tol"] = 1e-018
parameters["emtol"] = 1e-015

parameters["registrationMethod"] = 'EM_GRBF'

parameters["iterationsStart"] = 2
parameters["maxEmIterations"] = 2
parameters["sigmaStart"] = 2
parameters["lambdaStart"] = 0.001
parameters["beta"] = 1
parameters["anneal"] = 0.99



outModel = slicer.vtkMRMLModelNode()
slicer.mrmlScene.AddNode( outModel )

parameters["transformedModel"] = outModel.GetID()

gmmRegistration = slicer.modules.gmmregistration
startTime = time.time()
slicer.cli.run(gmmRegistration, None, parameters)
endTime = time.time()
print(endTime-startTime)
