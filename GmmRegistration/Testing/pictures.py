from PIL import Image
import glob
from PIL import ImageDraw
from PIL import ImageFont
import string
import numpy
import os
########################################################################################################
Run = "Run6"
########################################################################################################
folder_res = "C:\\Mike\Git\\calavera-3d-slicer\\GmmRegistration\\Data\\Input\\smallPiecesRegistration\\optimization" #%Run
class PackNode(object):
    """
    Creates an area which can recursively pack other areas of smaller sizes into itself.
    """
    def __init__(self, area):
        #if tuple contains two elements, assume they are width and height, and origin is (0,0)
        if len(area) == 2:
            area = (0,0,area[0],area[1])
        self.area = area
    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, str(self.area))
    def get_width(self):
        return self.area[2] - self.area[0]
    width = property(fget=get_width)

    def get_height(self):
        return self.area[3] - self.area[1]
    height = property(fget=get_height)
    def insert(self, area):
        if hasattr(self, 'child'):
            a = self.child[0].insert(area)
            if a is None: return self.child[1].insert(area)
            return a
        area = PackNode(area)
        if area.width <= self.width and area.height <= self.height:
            self.child = [None,None]
            self.child[0] = PackNode((self.area[0]+area.width, self.area[1], self.area[2], self.area[1] + area.height))
            self.child[1] = PackNode((self.area[0], self.area[1]+area.height, self.area[2], self.area[3]))
            return PackNode((self.area[0], self.area[1], self.area[0]+area.width, self.area[1]+area.height))

if __name__ == "__main__":
    format = 'RGBA'
    #size of the image we are packing into
    #size = 7100,2800
    #get a list of PNG files in the current directory
    picDirectory = 'C:\\Mike\Git\\calavera-3d-slicer\\GmmRegistration\\Data\\Input\\smallPiecesRegistration\\optimization\\'

    filename = 'sigma' '*' + '_lambda' + '*' + '_iterations'+ '*' + '_beta' + '*' + '_anneal' + '*' + '_elapsedTime*.png'
    names = glob.glob(picDirectory + filename)

    #names = [picDirectory+'rms14.7595_sigma0.1_lambda0.001_iterations10_beta0.001_anneal0.1_elapsedTime20.0950000286.png', picDirectory+'rms14.7593_sigma0.1_lambda0.001_iterations10_beta0.001_anneal0.5_elapsedTime30.1000001431.png'] #glob.glob("%s/*.png" %folder_res)
    #create a list of PIL Image objects, sorted by size
    images = sorted([(i.size[0]*i.size[1], name, i) for name,i in ((x,Image.open(x).convert(format)) for x in names)])

    #renaming
    #for name in names:
    #    rmsLocation = string.find(name, 'rms')
    #    if rmsLocation>0:
    #        sigmaLocation = string.find(name, '_sigma')
    #        if sigmaLocation > rmsLocation:
    #            extensionLocation = string.find(name, '.png')
    #            os.rename(name,picDirectory+name[sigmaLocation+1:extensionLocation]+'_'+name[rmsLocation:sigmaLocation]+'.png')

    names = glob.glob(picDirectory + filename)

    screenShotSize = 463, 568 # 515

    annealSize = screenShotSize[0]*4, screenShotSize[1]
    betaSize = screenShotSize[0]*4, screenShotSize[1]*4
    iterationsSize = screenShotSize[0]*4*4, screenShotSize[1]*4
    lambdaSize = screenShotSize[0]*4*4, screenShotSize[1]*4*4

    #treeSigma = PackNode(size)
    treeLambda = PackNode(lambdaSize)
    treeIterations = PackNode(iterationsSize)
    treeBeta = PackNode(betaSize)
    treeAnneal = PackNode(annealSize)

    #imageSigma = Image.new(format, size)
    imageLambda = Image.new(format, lambdaSize)
    imageIterations = Image.new(format, iterationsSize)
    imageBeta = Image.new(format, betaSize)
    imageAnneal = Image.new(format, annealSize)

    #insert each image into the PackNode area



    # parsing the filename

    name = images[0][1]

    sigmaLocation = string.find(name, '_sigma')
    lambdaLocation = string.find(name, '_lambda')
    iterationsLocation = string.find(name, '_iterations')
    betaLocation = string.find(name, '_beta')
    annealLocation = string.find(name, '_anneal')
    elapsedTimeLocation = string.find(name, '_elapsedTime')
    extensionLocation = string.find(name, '.png')

    sigmaValue = name[sigmaLocation + 6:lambdaLocation]
    lambdaValue = name[lambdaLocation + 7:iterationsLocation]
    iterationsValue = name[iterationsLocation + 11:betaLocation]
    betaValue = name[betaLocation + 5:annealLocation]
    annealValue = name[annealLocation + 7:elapsedTimeLocation]
    elapsedTimeValue = name[elapsedTimeLocation + 12:extensionLocation]


    currentSigmaValue = sigmaValue
    currentLambdaValue = lambdaValue
    currentIterationsValue = iterationsValue
    currentBetaValue = betaValue
    currentAnnealValue = annealValue
    currentElapsedTimeValue = elapsedTimeValue

    sigmaCount = 0
    fileIndex = -1

    for area, name, img in images:

        fileIndex = fileIndex + 1
        draw = ImageDraw.Draw(img)
        #font = ImageFont.truetype("D:\hp\Apps\APP25763\src\MSWorks\en\msworks\Fonts\ArialN.TTF", 150)

        #parsing the filename
        sigmaLocation = string.find(name, 'sigma')
        lambdaLocation = string.find(name, '_lambda')
        iterationsLocation = string.find(name, '_iterations')
        betaLocation = string.find(name, '_beta')
        annealLocation = string.find(name, '_anneal')
        elapsedTimeLocation = string.find(name,'_elapsedTime')
        extensionLocation = string.find(name,'.png')

        sigmaValue = name[sigmaLocation+5:lambdaLocation]
        lambdaValue = name[lambdaLocation+7:iterationsLocation]
        iterationsValue = name[iterationsLocation+11:betaLocation]
        betaValue = name[betaLocation+5:annealLocation]
        annealValue = name[annealLocation+7:elapsedTimeLocation]
        elapsedTimeValue = name[elapsedTimeLocation+12:extensionLocation]


        beginningLocation = numpy.array((10,10))
        fontSize = 35
        offset = numpy.array((0,fontSize))
        #font = ImageFont.load("C:/Windows/Fonts/ARIALN.TTF")
        myFont = ImageFont.truetype("C:/Windows/Fonts/ARIALN.TTF", fontSize)
        draw.text(beginningLocation.tolist(), 's=' + sigmaValue, font=myFont)
        draw.text((beginningLocation + offset).tolist(), 'l=' + lambdaValue, font=myFont)
        draw.text((beginningLocation + 2 * offset).tolist(), 'i=' + iterationsValue, font=myFont)
        draw.text((beginningLocation + 3 * offset).tolist(), 'b=' + betaValue, font=myFont)
        draw.text((beginningLocation + 4 * offset).tolist(), 'a=' + annealValue, font=myFont)
        #draw.text((beginningLocation + 5 * offset).tolist(), 't=' + elapsedTimeValue, font=myFont)


        #note to self I am still implimenting this recursive strategy to create my panneled image


#        if currentLambdaValue == lambdaValue:
#            uv = treeSigma.insert(img.size)
#            if uv is None: raise ValueError('Pack size too small.')
#            #treeSigma.paste(img,uv.area)
#            imageSigma.paste(img, uv.area)
#            imageSigma.show()
#            print sigmaValue
#        else:
#            if currentIterationsValue == iterationsValue:
#                uv = treeLambda.insert(imageSigma.size)
#                if uv is None: raise ValueError('Pack size too small.')
#                imageLambda.paste(imageSigma, uv.area)
#
#                imageSigma = Image.new(format, size)
#                treeSigma = PackNode(size)
#            else:
#                if currentBetaValue == betaValue:
#                    uv = treeIterations.insert(imageLambda.size)
#                    if uv is None: raise ValueError('Pack size too small.')
#                    imageIterations.paste(imageLambda, uv.area)
#
#                    imageLambda = Image.new(format, size)
#                    treeLambda = PackNode(size)
#                else:
#                    if currentAnnealValue == annealValue:
#                        uv = treeBeta.insert(imageIterations.size)
#                        if uv is None: raise ValueError('Pack size too small.')
#                        imageBeta.paste(imageIterations, uv.area)
#                    else:
#                        imageBeta.save(folder_res + "PanelImage_a=" + annealValue + ".png")
#                        imageBeta = Image.new(format, size)
#                        treeBeta = PackNode(size)



        if currentBetaValue != betaValue:

            uv = treeBeta.insert(imageAnneal.size)
            if uv is None: raise ValueError('Pack size too small.')
            imageBeta.paste(imageAnneal, uv.area)

            if currentIterationsValue != iterationsValue:

                uv = treeIterations.insert(imageBeta.size)
                if uv is None: raise ValueError('Pack size too small.')
                imageIterations.paste(imageBeta, uv.area)

                if currentLambdaValue != lambdaValue:

                    uv = treeLambda.insert(imageIterations.size)
                    if uv is None:
                        raise ValueError('Pack size too small.')
                    imageLambda.paste(imageIterations, uv.area)

                    if currentSigmaValue != sigmaValue:
                        imageLambda.save(folder_res + "PanelImage_s=" + currentSigmaValue + ".png")
                        imageLambda = Image.new(format, lambdaSize)
                        treeLambda = PackNode(lambdaSize)


                    imageIterations = Image.new(format, iterationsSize)
                    #probably should add the new one
                    treeIterations = PackNode(iterationsSize)



                imageBeta = Image.new(format, betaSize)
                treeBeta = PackNode(betaSize)



            imageAnneal = Image.new(format, annealSize)
            treeAnneal = PackNode(annealSize)

        uv = treeAnneal.insert(img.size)
        if uv is None: raise ValueError('Pack size too small.')
        # treeSigma.paste(img,uv.area)
        imageAnneal.paste(img, uv.area)

        currentSigmaValue = sigmaValue
        currentLambdaValue = lambdaValue
        currentIterationsValue = iterationsValue
        currentBetaValue = betaValue
        currentAnnealValue = annealValue
        currentElapsedTimeValue = elapsedTimeValue




    #image.save("%s/%s.png" %(folder_res,Run))
