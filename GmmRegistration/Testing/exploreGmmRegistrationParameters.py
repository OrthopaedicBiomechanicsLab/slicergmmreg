#The idea is that this script is run from the command line with something like

# execfile('C:\\Mike\\Git\\calavera-3d-slicer\\GmmRegistration\\Testing\\exploreGmmRegistrationParameters.py')

import time
import csv
import vtk
import glob
import sys


import pdb


slicer.util.loadScene('C:\Mike\Git\calavera-3d-slicer\GmmRegistration\Data\Input\smallPiecesRegistration\OptimizationScene.mrml')
#we will assume that data is loaded in the scene
#dataDirectory = os.path.dirname(os.path.dirname(__file__))
#slicer.util.loadModel(dataDirectory + '\\Input\\smallPiecesRegistration\\FillerModel1.vtk')
fillerModelNode = getNode('FillerModel#1')
#slicer.util.loadModel(dataDirectory + '\\Input\\smallPiecesRegistration\\Model_1_jake.vtk')
skullNode = getNode('Model_1_jake_2step_Clipped')
destinationDirectory = 'C:\\Mike\\Git\\calavera-3d-slicer\\GmmRegistration\\Data\\Input\\smallPiecesRegistration\\optimization\\'
targetSkullNode = getNode('BA_14514712_PostOp_SkullFilled_transformed_Clipped')

parameters = {}
parameters["model"] = fillerModelNode.GetID()
parameters["sceneGmm"] = skullNode.GetID()
outModel = slicer.vtkMRMLModelNode()
slicer.mrmlScene.AddNode( outModel )

parameters["transformedModel"] = outModel.GetID()
parameters["normalize"] = 0
parameters["outliers"] = 0
parameters["tol"] = 1e-018
parameters["emtol"] = 1e-015

parameters["registrationMethod"] = 'EM_GRBF'

iterationsList = [2, 5, 10, 50]
sigmaList = [2, 1, 0.5, 0.1]
lambdaFractionList = [1, 0.1, 0.01, 0.001]
betaList = [1, 0.1, 0.01, 0.001]
annealList = [0.99, 0.97, 0.5, 0.1]





gmmRegistration = slicer.modules.gmmregistration
modelDistance = slicer.modules.modeltomodeldistance

parametersModelDistance = {}
parametersModelDistance["vtkFile1"] = outModel.GetID()
parametersModelDistance["vtkFile2"] = targetSkullNode.GetID()
distanceOutModel = slicer.vtkMRMLModelNode()
slicer.mrmlScene.AddNode( distanceOutModel )
parametersModelDistance["vtkOutput"] = distanceOutModel.GetID()
parametersModelDistance["distanceType"] = 'signed_closest_point'
parametersModelDistance["targetInFields"] = False

for iterationElement in iterationsList:
    parameters["iterationsStart"] = iterationElement
    parameters["maxEmIterations"] = iterationElement
    for sigmaElement in sigmaList:
        parameters["sigmaStart"] = sigmaElement
        for lambdaElement in lambdaFractionList:
            parameters["lambdaStart"] = sigmaElement*lambdaElement
            for betaElement in betaList:
                parameters["beta"] = betaElement
                for annealElement in annealList:

                    filename = '*_sigma' + str(sigmaElement) + '_lambda' + str(
                        lambdaElement) + '_iterations' + str(iterationElement) + '_beta' + str(
                        betaElement) + '_anneal' + str(annealElement) + '_elapsedTime*.png'
                    fileList = glob.glob(destinationDirectory+filename)
                    #pdb.set_trace()
                    if len(fileList) ==0:
                        parameters["anneal"] = annealElement
                        startTime = time.time()

                        gmmRegistrationCliResult = slicer.cli.run(gmmRegistration, None, parameters)

                        elapsedTime = time.time() - startTime

                        #pdb.set_trace()
                        while (gmmRegistrationCliResult.GetStatusString() == 'Running' or gmmRegistrationCliResult.GetStatusString() == 'Scheduled' or gmmRegistrationCliResult.GetStatusString() == 'Cancelling' or gmmRegistrationCliResult.GetStatusString() == 'Completing'):
                            #if round(elapsedTime/10)%2:
                            #    print gmmRegistrationCliResult.GetStatusString()+" gmmRegistration... " + str(elapsedTime)
                            #    sys.stdout.flush()
                            slicer.util.delayDisplay("gmmReg " + gmmRegistrationCliResult.GetStatusString() + "... %d" % elapsedTime,5000)
                            elapsedTime = time.time() - startTime
                        #endTime = time.time()
                        #elapsedTime = endTime - startTime

                        if gmmRegistrationCliResult.GetStatusString() == 'Completing':
                            gmmRegistrationCliResult.SetStatus(gmmRegistrationCliResult.Completed)
                            print outModel.GetPolyData().GetNumberOfPoints()
                            pdb.set_trace()

                        if gmmRegistrationCliResult.GetStatusString() == 'Completed' and outModel.GetPolyData().GetNumberOfPoints()!=0:

                            modelDistanceResult = slicer.cli.run(modelDistance, None, parametersModelDistance)
                            elapsedTime = time.time() - startTime

                            distanceStartTime = time.time()
                            distanceElapsedTime = time.time()-distanceStartTime
                            while (modelDistanceResult.GetStatusString() == 'Running' or modelDistanceResult.GetStatusString() == 'Scheduled' or modelDistanceResult.GetStatusString() == 'Cancelling' or modelDistanceResult.GetStatusString() == 'Completing') and  distanceElapsedTime  < 30:
                               #if round(elapsedTime / 10) % 2:
                               #     print modelDistanceResult.GetStatusString()+" modelDistance... " + str(elapsedTime)
                               #     sys.stdout.flush()
                               slicer.util.delayDisplay("model distance " + gmmRegistrationCliResult.GetStatusString() + "... %d" % distanceElapsedTime, 5000)
                               distanceElapsedTime = time.time() - startTime

                            if modelDistanceResult.GetStatusString() == 'Completing':
                                rmsDistance = modelDistanceResult.GetParameterAsString('rmsModelDistance')
                                print rmsDistance
                                pdb.set_trace()


                            #pdb.set_trace()
                            rmsDistance = modelDistanceResult.GetParameterAsString('rmsModelDistance')

                            layoutManager = slicer.app.layoutManager()
                            view1 = layoutManager.threeDWidget(0).threeDView()
                            w2i1 = vtk.vtkWindowToImageFilter()
                            w2i1.SetInput(view1.renderWindow())
                            w2i1.Update()
                            image1 = w2i1.GetOutput()
                            screenShotNode2 = slicer.vtkMRMLAnnotationSnapshotNode()
                            screenShotNode2.SetScreenShot(image1)
                            slicer.mrmlScene.AddNode(screenShotNode2)

                            filename = 'rms'+str(rmsDistance)+'_sigma'+str(sigmaElement)+'_lambda'+str(lambdaElement)+'_iterations'+str(iterationElement)+'_beta'+str(betaElement)+'_anneal'+str(annealElement)+'_elapsedTime'+str(elapsedTime)+'.png'
                            slicer.util.saveNode(screenShotNode2, destinationDirectory + filename)
                        else:
                            filename= 'failed_'+'_sigma'+str(sigmaElement)+'_lambda'+str(lambdaElement)+'_iterations'+str(iterationElement)+'_beta'+str(betaElement)+'_anneal'+str(annealElement)+'_elapsedTime'+str(elapsedTime)+'.png'
                            csvFile = open(destinationDirectory + filename, 'wb')
                            csvFileWriter = csv.writer(csvFile)
                            csvFileWriter.writerow('failed')
                            csvFile.close()
