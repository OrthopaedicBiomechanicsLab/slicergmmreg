REM Note this assumes that the Slicer superbuild is located in .\s49r
set "VSCMD_START_DIR=%CD%"
call "%VS140COMNTOOLS%VsDevCmd.bat"
md slicergmmreg-build
cd slicergmmreg-build

cmake -DSlicer_DIR=%cd%\..\s49r\Slicer-build -G"Visual Studio 14 2015 Win64" C:\Mike\Git\SlicerGmmReg
cmake --build . --config Release
