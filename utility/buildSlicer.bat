REM this file is meant to build slicer in the current dir
REM it checks out a specific commit that has been show to work with SlicerGMMReg

REM install qt
REM powershell downloadQt.ps1
REM qt-opensource-windows-x86-5.10.0.exe  --script %~dp0\qt-installer-noninteractive.qs

@call "%VS140COMNTOOLS%VsDevCmd.bat"
git clone https://github.com/Slicer/Slicer.git
cd Slicer
git checkout 4449f7e8d6793977198b2b928f16f556a587e95a
cd ..
REM Slicer_Verision4_Release
md s4r
cd s4r

cmake 	-DQT_QMAKE_EXECUTABLE:PATH=C:/Qt/Qt5.10.0/5.10.0/msvc2015/bin/qmake.exe ^
	-DQT_VERSION=5.10.0 ^
	-DQt5_DIR:PATH=C:\Qt\Qt5.10.0\5.10.0\msvc2015_64\lib\cmake\Qt5 ^
	-DSlicer_REQUIRED_QT_VERSION:STRING=5.10.0 ^
	-DSlicer_USE_VTK_DEBUG_LEAKS:BOOL=OFF ^
	-DSlicer_VTK_VERSION_MAJOR:STRING=9 ^
	-G"Visual Studio 14 2015 Win64" ..\Slicer
	
cmake --build . --config Release